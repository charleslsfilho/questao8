package questao8;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Integer a;
		Integer b;
		Integer c;
		
		Scanner reader = new Scanner(System.in);
		System.out.println ("Digite o valor de a");
		a = reader.nextInt();
		System.out.println ("Digite o valor de b");
		b = reader.nextInt();
		char[] digitosA = String.valueOf( a ).toCharArray();
		char[] digitosB = String.valueOf( b ).toCharArray();
		StringBuilder valueC = new StringBuilder();
		int countLoop = digitosA.length >= digitosB.length ? digitosA.length : digitosB.length;
		for (int i = 0; i < countLoop; i++) {
			valueC.append(digitosA.length > i ? digitosA[i] : "");
			valueC.append(digitosB.length > i ? digitosB[i] : "");

		}
		c = Integer.parseInt(valueC.toString());
		System.out.println(c > 1000000 ? -1 : c);

		
	}
	
}
